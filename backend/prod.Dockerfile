FROM node:5.12
COPY ./package.json /deps/package.json
WORKDIR /deps
RUN npm i && npm i -g pm2
VOLUME /deps/app
WORKDIR /deps/app
CMD pm2 start --no-daemon process.json