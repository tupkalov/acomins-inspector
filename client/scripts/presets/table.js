module.exports = {

    template () {
            return `
table.table
    thead.thead-inverse
        tr
            each header in fields["${SUBNAME}"]
                th #{header}
    tbody
        each row in data["${SUBNAME}"]
            tr
                each header in fields["${SUBNAME}"]
                    td #{row[header]}`;
    }

}