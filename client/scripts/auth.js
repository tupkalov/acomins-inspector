const LOGIN_URL = "/api/signin";

const modal = require('./modal')


function initAuth () {
	const authEl = document.querySelector('.auth');

	if (!authEl) return;

	const form = authEl.querySelector('form');
	form.addEventListener('submit', event => {
		event.preventDefault();
		submit(event);
	})
}

function loginSuccess (redirect) {
	location.assign(redirect);
}


function submit({ target : form }) {
	const { login : { value : login }, password : { value : password } } = form

	// Получаем логин и пароль
	fetch(LOGIN_URL, {
		body : JSON.stringify({login, password}),
		method : "POST",
		credentials : "include",
		headers: {
		  'Accept': 'application/json',
		  'Content-Type': 'application/json'
		}
	}).then(response => {
		if (response.status === 200)
			return response.json();	
		else
			throw new Error(response.statusText)
	}).then(
		({ type, message, redirect }) => {
			switch (type) {
				case 'ok':
					loginSuccess(redirect);
					break;

				case 'error': {
					let text = lang.signinError(message);
					modal({ text });
				}
			}
		},

		(error) => {
			console.error(error);
			let text = lang.signinError(error.message);
			modal({ text })
		}
	)
}

export default initAuth;