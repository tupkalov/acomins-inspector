module.exports = {
	entry: '/input/scripts/entry.js',
	output: {
		path : '/output',
		filename : 'bundle.js',
	},
	devtool: 'source-map',
	watch : true
};