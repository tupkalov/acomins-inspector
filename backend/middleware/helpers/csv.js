module.exports = function (data, fields, name) {
    	// Делаем заголовки
    	const CSVfields = fields[name],
    		  CSVdata = data[name];

    	if (!CSVdata)
    		throw new AppError('CSV:NoDataForThisName', { info : { name } });


    	const outputData = [
    		CSVfields,
    		...CSVdata.map(row => 
    			CSVfields.map(field => 
    				row[field]
    			)
    		)
    	];

    	const CSVString = outputData.map(row => 
    		row.map(cell => `"${typeof cell === "string" ? cell.replace('"', '""') : cell}"`).join(',')
    	).join('\n');

    	return encodeURI("data:text/csv;charset=utf-8," + CSVString)
}