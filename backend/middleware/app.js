const Router = require('express').Router;
const db = require('../db')
const outerDb = require('../outer-db');

const pug = require('pug')
const findTemplateByName = require('./helpers/templateHelper').findTemplateByName

const csvHelper = require('./helpers/csv')

const pugNameReg = /(?:\/|\\)([1-9a-z_]+)\.pug$/;

const SUBS_RELATIVE = process.env.ACOMINS_IN_DOCKER ? '/deps/mount/subs/.pug' : __rootdir + '/../mount/subs/.pug';

global.ApplicationError = HttpError.extend('Application');

const router = module.exports = new Router();

router.use(generatorWrap(function* (req, res, next) {
	if (!req.session.logged) return res.redirect('/');

	const user = res.locals.user = req.session.user
	const templates = res.locals.templates = req.session.rights === "admin"
		? yield db.getTemplates()
		: yield db.getTemplatesByNames(user.templates || []);
	next();
}))

router.get('/', generatorWrap(function* (req, res, next) {
	res.render('app')
}))

router.get('/:templateName', generatorWrap(function* (req, res, next) {
	const templateName = req.params.templateName;
	// Permissions
	const user = req.session.user,
		  rights = req.session.rights;
	if (rights !== "admin" && (user.templates || []).indexOf(templateName) === -1)
		throw new ApplicationError("app/template:noPermissions")

	const template = yield findTemplateByName(templateName);
	if (!template)
		throw new ApplicationError("app/template:templateNotFound")

	res.locals.template = template;

	const result = pug.compileClientWithDependenciesTracked(template.string, {
		filename : SUBS_RELATIVE
	});

	const evalMethod = result.body, 
		  dependencies = result.dependencies;

	//  Разбираем зависимости
	const subNames = dependencies.map(name => {
		if (!pugNameReg.test(name))
			throw new ApplicationError('app/template:wrongSubFormat', { info : { templateName, subName : name }})
		const subName = pugNameReg.exec(name)[1];
		return subName;
	})


	// Вытаскиваем запросы
	const queries = yield db.getQueriesByNames(subNames);

	const data = res.locals.data = {};
	const fields = res.locals.fields = {};

	yield Promise.all(queries.map((row) => {
		const querystring = row.querystring, 
			  name  = row.name;

		const promise = outerDb.query(querystring)
		promise.then((resp) => {
			const results = resp.results,
				  resultsFields = resp.fields
			data[name] = results;
			fields[name] = resultsFields.map(fieldData => fieldData.name);
		})

		return promise;
	}))

	// Рендерим
	const locals = res.locals;
	
	// assignHelpers
	Object.assign(res.locals, {
		getCSVhref : csvHelper.bind(res.locals, data, fields)
	});
	
	const renderMethod = (() => {
		eval(evalMethod)
		return template;
	})();

	var newData = {};
	for (var name in data) {
		var arr = data[name].map(row => {
			var ret = {};
			for (var key in row) {
				var val = row[key];
				if ((val instanceof Buffer) || (typeof val === "object" && val.type === "Buffer"))
					val = val.toString();
				ret[key] = val;
			}
			return ret
		});

		newData[name] = arr;
	}
	
	const include = `<script type="text/javascript">
		window.data = JSON.parse('${JSON.stringify(newData).replace('\'', '\\\'')}');
		window.fields = JSON.parse('${JSON.stringify(fields).replace('\'', '\\\'')}');
	</script>` 

	const html = res.locals.html = include + renderMethod(locals);

	res.render('app')
}))