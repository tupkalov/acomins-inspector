const mysql      = require('mysql');

const ACOMINS_OUTER_DB_LOGIN = process.env.ACOMINS_OUTER_DB_LOGIN,
		ACOMINS_OUTER_DB_PASSWORD = process.env.ACOMINS_OUTER_DB_PASSWORD,
		ACOMINS_OUTER_DB_HOST = process.env.ACOMINS_OUTER_DB_HOST,
		ACOMINS_OUTER_DB_PORT = process.env.ACOMINS_OUTER_DB_PORT,
		ACOMINS_OUTER_DB_DATABASE = process.env.ACOMINS_OUTER_DB_DATABASE

var connection;

function getConnection () {
	return connection ? Promise.resolve(connection) : new Promise((resolve, reject) => {
		connection = mysql.createConnection({
			host     : ACOMINS_OUTER_DB_HOST,
			user     : ACOMINS_OUTER_DB_LOGIN,
			password : ACOMINS_OUTER_DB_PASSWORD,
			database : ACOMINS_OUTER_DB_DATABASE
		});

		connection.connect(error => {
			if (error) throw error;
			else resolve(connection)
		});

		connection.on('error', error => {
			if (error.fatal) {
				connection = null;
			}
		})

	})
}

module.exports = {
	query (sql) {
		return getConnection().then(() => 
			new Promise((resolve, reject) => {
				connection.query(sql, function (error, results, fields) {
					if (error)
						reject(error);
					else{
						resolve({ results, fields})
					}
				});
			}))
	}
}