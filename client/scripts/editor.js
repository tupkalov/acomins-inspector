global.EDITORS = new Map;

export default () => {
    Array.prototype.slice.call(document.querySelectorAll("textarea + .code")).forEach(codeEl => {

	    var editor = ace.edit(codeEl);
	    var { theme = "chrome", mode = "jade" } = codeEl.dataset;
	    editor.setTheme(`ace/theme/${theme}`);
	    editor.getSession().setMode(`ace/mode/${mode}`);

		var textarea = codeEl.previousSibling;
		textarea.style.display = 'none';
		editor.getSession().setValue(textarea.value);
		editor.getSession().on('change', function(){
			textarea.value = editor.getSession().getValue();
		});
		editor.setOptions({
			fontSize: "14pt",
			fontWeight: 'normal' 
		});

		EDITORS.set(codeEl, editor)
    });
}