const authasher = require('authasher');
const db = require(`${__rootdir}/db`);

const FOLDER = '/deps/mount/subs/';
const fs = require('fs');

const subHelper = require('../helpers/subHelper')


const fetchSubsFiles = subHelper.fetchSubsFiles,
	fetchQueries = subHelper.fetchQueries,
	fetchSubs = subHelper.fetchSubs,
	createSub = subHelper.createSub,
	changeTemplateByName = subHelper.changeTemplateByName,
	deleteTemplateByName = subHelper.deleteTemplateByName,
	findTemplateByName = subHelper.findTemplateByName;

module.exports = router => {





router.use('/subs', generatorWrap(function*(req, res, next) {

	const subs = yield fetchSubs()
	res.locals.subs = subs;
	res.locals.currentPage = 'subs';
	next();
}))

router.get('/subs', generatorWrap(function*(req, res, next) {
	res.render('allSubs');
}))



router.use('/subs/create', (req, res, next) => {
	res.locals.sub = { id : 'create' };
	res.locals.template = '';
	next()
})

router.get('/subs/create', generatorWrap(function* (req, res, next) {
	res.render('createSub');
}))

router.post('/subs/create', generatorWrap(function* (req, res, next) {
	const name = req.body.name,
		querystring = req.body.querystring,
		template = req.body.template

	try {
		if (typeof name !== "string" || !(name = name.trim()).length)
			throw new AdminError('subs/create:wrongName');

		const nameTry = res.locals.subs.byName[name];
		if (nameTry)
			throw new AdminError('subs/create:nameAlreadyExists')

		if (typeof querystring !== "string")
			throw new AdminError('subs/create:noQuery');

		if (typeof template !== "string")
			throw new AdminError('subs/create:wrongTemplate');

		const sub = yield createSub({ name, querystring, template });

		res.locals.sub = sub;


		res.redirect(`/admin/subs/${name}`)
	} catch (error) {
		console.log(error);

		const localMessage = lang.subCreateError(error.message)
		res.locals.error = localMessage;
		res.locals.name = name;
		res.locals.querystring = querystring;
		res.locals.template = template;

		res.render('createSub');
	}

}))

router.use('/subs/:name', generatorWrap(function* (req, res, next) {
	const sub = res.locals.subs.byName[req.params.name];
	if (!sub) return res.redirect('/admin/subs');

	sub.template = yield findTemplateByName(sub.name)
	res.locals.sub = sub;	
	next()
}))


router.get('/subs/:name', generatorWrap(function* users (req, res, next) {
	res.render('sub');
}))

router.post('/subs/:name', generatorWrap(function* users (req, res, next) {
	const querystring = req.body.querystring,
		template = req.body.template,
		deleteSub = req.body.deleteSub;

	if (querystring) {
		try {
			if (typeof querystring !== "string")
				throw new AdminError('subChange:wrongQueryString');

			if (querystring !== res.locals.sub.querystring) {

				yield db.createQueryByName(res.locals.sub.name, { querystring });
				Object.assign(res.locals.sub, { querystring });
			}
		} catch (error) {
			console.log(error);
			res.locals.querystringError = lang.subChangeError(error.message);
		}


	} else if (template) {
		try {
			if (typeof template !== "string")
				throw new AdminError('subChange:wrongTemplate');

			if (template !== res.locals.sub.template) {
				yield changeTemplateByName(res.locals.sub.name, { template });
				Object.assign(res.locals.sub, { template });
			}
		} catch (error) {
			console.log(error);
			res.locals.templateError = lang.subChangeError(error.message);
		}

	} else if (deleteSub) {
		const name = res.locals.sub.name;
		yield Promise.all([ deleteTemplateByName(name), db.deleteQueryByName(name) ]);
		res.redirect('/admin/subs')
	}

	res.render('sub');
}))

}