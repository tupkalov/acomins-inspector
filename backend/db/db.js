const lokijs = require('lokijs');
const DbError = require('app-error').extend('Db');


const errors = [
	'getUserByLogin:userNotFound',
	'getUserById:userNotFound',
]



var db;
const collections = {};


//  START
const loadPromise = new Promise((resolve, reject) => {
	db = new lokijs(DB_JSON_PATH, {
		verbose : true,
		autosave : true,
		autoload : true,
		autoloadCallback (error) {
			if (error) reject(error);

			resolve(db);
		} 
	})
})

function getCollectionOptions (name) {
	switch (name) {
		case 'users':
			return {
				indices : ['_id']
			}

		default:
			return {};
	}
}

function getCollection (name) {
	if (collections[name])
		return Promise.resolve(collections[name]);
	else
		return loadPromise.then(db => {
			return collections[name] = db.getCollection(name) || db.addCollection(name, getCollectionOptions(name));
		});
} 

function saveDatabase () {
	return new Promise((resolve, reject) => {
		loadPromise.then(db => {
			return db.saveDatabase(error => {
				if (error)
					reject(error)
				else 
					resolve(error)
			})
		})
	});
}

const getUsers = co.wrap(function* () {
	var collection = yield getCollection('users');
	const users = yield collection.find();
	users.forEach(data => (data.id = data.$loki));
	return users
})

const getUserByLogin = co.wrap(function* (login) {
	var collection = yield getCollection('users');
	const user = collection.findOne({ login });
	if (user) user.id = user.$loki;
	return user;
})

const getUserById = co.wrap(function* (id) {
	var collection = yield getCollection('users');
	const user = collection.get( id );
	if (user) user.id = user.$loki;
	return user;
})

const createUser = co.wrap(function* (query) {
	const login = query.login,
		  passwordHash = query.passwordHash,
		  name = query.name;

	const collection = yield getCollection('users')
	const id = collection.insert({ login, passwordHash, name }).$loki
	return { id }
})

const changeUserById = co.wrap(function* (id, data) {
	const collection = yield getCollection('users')
	const user = yield collection.get( id );
	Object.assign(user, data)
	yield collection.update(user);
	return user;
})


const deleteUserById = co.wrap(function* (id) {
	const collection = yield getCollection('users')
	const user = yield collection.get( id );
	yield collection.remove(user);
})

const getQueries = co.wrap(function* (userId) {
	const collection = yield getCollection('queries');
	return collection.find();
})

const getQueriesByNames = co.wrap(function* (names) {
	const collection = yield getCollection('queries');
	return collection.find({ name : { $in : names }});
})

const createQueryByName = co.wrap(function* (name, query) {
	const querystring = query.querystring;
	const collection = yield getCollection('queries')
	const exists = collection.findOne({ name });
	if (exists)
		collection.update(Object.assign(exists, { name, querystring }))
	else {
		collection.insert({ name, querystring })
	}
})

const deleteQueryByName = co.wrap(function* (name) {
	const collection = yield getCollection('queries')
	const query = yield collection.findOne( { name } );
	query && collection.remove(query);
})


const getTemplates = co.wrap(function* () {
	const collection = yield getCollection('templates');
	return collection.find();
})

const getTemplateByName = co.wrap(function* (name) {
	const collection = yield getCollection('templates');
	return collection.findOne({ name });
})

const changeTemplateByName = co.wrap(function* (name, data) {
	const collection = yield getCollection('templates')
	const exists = collection.findOne({ name });
	if (exists)
		collection.update(Object.assign(exists, data))
	else {
		collection.insert(Object.assign({ name }, data))
	}
})

const deleteTemplateByName = co.wrap(function* (name) {
	const collection = yield getCollection('templates')
	const template = yield collection.findOne( { name } );
	template && collection.remove(template);
})

const getTemplatesByNames = co.wrap(function* (names) {
	const collection = yield getCollection('templates')
	return collection.find({ name : { $in : names }});
	
})


module.exports = {
	getUsers,

	getUserByLogin,

	getUserById,

	getQueries,

	createQueryByName,

	createUser,

	changeUserById,

	deleteUserById,

	deleteQueryByName,

	getCollection,

	getTemplates,

	getTemplateByName,

	changeTemplateByName,

	deleteTemplateByName,

	getTemplatesByNames,

	getQueriesByNames
}