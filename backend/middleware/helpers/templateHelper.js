const db = require(`${__rootdir}/db`)
const fs = require('fs')


const FOLDER = process.env.ACOMINS_IN_DOCKER ? '/deps/mount/templates/' : __rootdir + '/../mount/templates/';

const fetchTemplatesFiles = co.wrap(function* () {
	const fetchObject = {}, fetchArray = [];
	const filesPromise = new Promise((resolve, reject) => {
		fs.readdir(FOLDER, (err, files) => {
			if (err) 
				return reject(err);

			resolve(files
				.filter(file => {
					return file.endsWith('.pug')
				})
				.map(file => {
					return file.split('.pug')[0]
				}));
		})
	})

	const dbPromise = db.getTemplates();


	const arr = yield { files : filesPromise, dbData :  dbPromise };

	const files = arr.files,
		  dbData = arr.dbData;

	Object.defineProperty(fetchArray, 'byName', {
		enumarable : false,
		value : fetchObject,
		writable : false
	})

	files.forEach(name => {
		fetchArray.push(
			fetchObject[name] = { name }
		)
	})

	dbData.forEach(template => {
		const name = template.name,
			  label = template.label;

		if (fetchObject[name])
			fetchObject[name].label = label;

		else 
			fetchArray.push(
				fetchObject[name] = { name, label }
			)
	})
	return fetchArray;
})


const createTemplate = co.wrap(function* (data) {

	yield changeTemplateByName(data.name, data);

	return data;
})

function changeTemplateByName (name, query) {
	const string = query.string,
		  label = query.label;
		  
	return Promise.all([
		string ? new Promise((resolve, reject) => 
				fs.open(`${FOLDER}${name}.pug`, 'w', (err, fd) => {
					if (err) reject(err)
					else resolve(fd)
				})
			)

		.then(fd => new Promise((resolve, reject) =>
			fs.writeFile(fd, string, (err) => {
				if (err) reject(err)
				else resolve()
			})
		)) : null,

		label ? db.changeTemplateByName(name, { label }) : null
	]);
}

function deleteTemplateByName (name) {
	return Promise.all([
		new Promise((resolve, reject) => 
			fs.unlink(`${FOLDER}${name}.pug`,  err => {
				err && console.error(err);
				resolve()
			})
		),

		db.deleteTemplateByName(name)
	])
}

const findTemplateByName = co.wrap(function* (name) {
	const arr = yield Promise.all([
		new Promise((resolve, reject) => 
			fs.open(`${FOLDER}${name}.pug`, 'r', (err, fd) => {
				if (err) resolve('')
				else fs.readFile(fd, (err, data) => {
					if (err) reject(err);
					else resolve(data);
				})
			})),

		db.getTemplateByName(name)
	]);

	const fileData = arr[0],
		  dbData = arr[1];

	return Object.assign({}, dbData, { string : fileData});
})

module.exports = {
	fetchTemplatesFiles,
	createTemplate,
	changeTemplateByName,
	deleteTemplateByName,
	findTemplateByName,
}