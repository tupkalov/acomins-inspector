module.exports = Object.assign(require('./webpack.config.js'), {
	watch : false,
	entry: ["babel-polyfill", '/input/scripts/entry.js'],
	module: {
		rules: [
			{ test: /\.js$/, loader: "babel-loader" }
		]
	}
});