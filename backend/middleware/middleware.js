global.HttpError = require('app-error').extend('http')

const api = require('./api')
const admin = require('./admin');
const appRouter = require('./app');



function bodyparser (app) {
	const BodyParser = require('body-parser');

	app.use(BodyParser.urlencoded({ extended: true }))
	app.use(BodyParser.json());
}

function session (app) {
    const session = require('express-session') ,
    	  JsonStore = require('express-session-json')(session);

	app.use(session({
	    secret: SECRET ,
	    resave: false,
	    saveUninitialized : true,
	    store: new JsonStore({ filename : SESSION_JSON_FILENAME, path : SESSION_JSON_PATH })
	}));
}
  
function render (app) {

	// view engine
	app.set('view engine', 'pug')
	app.set('views', VIEWS_PATH)
	app.disable('view cache');
}

module.exports = app => {
	bodyparser(app);
	session(app);
	render(app);


	app.get('/', index);
	app.use('/logout', (req, res, next) => {
		req.session.destroy(function (err) {
			if (err)
				next(err);
			else
				res.redirect('/')
		})
	})
	app.use('/admin', admin);
	app.use('/api', api);
	app.use('/app', appRouter);
	app.use(errorHandler);
}



//  PAGES
function index (req, res, next) {
	if (req.session.logged)
		res.redirect(req.session.rights === "admin" ? "/admin" : '/app')
	else
		res.render('index')
}

function errorHandler (error, req, res, next) {
	console.log('ERROR:')
	error.stack && console.log(error.stack)

	if (error instanceof HttpError) {
		var status = error.info && error.info.status || 200;
		var message = error.message;
	}else{
		var status = 500;
		var message = "InternalError";
	}

	res.status(status);
	Object.assign(res.locals, {
		status,
		message,
		error,
		isAdmin : req.session.rights === "admin"
	})

	res.render('error')
}