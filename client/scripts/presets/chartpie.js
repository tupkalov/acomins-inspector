import uid from '../helpers/uid';
import gen from 'color-generator';

const COUNT_COLORS = 30;


export default {
    template () {

        // Colors
        const PIECOLORS = [];
        const LIGHT_PIECOLORS = []

        for (let left = COUNT_COLORS; left; left--) {
            let color = gen();
            PIECOLORS.push(color.hexString());
            LIGHT_PIECOLORS.push(color.lighten(.1).hexString())
        }

        const id = uid();

        return `
canvas#${id}(width="400", height="200")
script.
    var colors = ${JSON.stringify(PIECOLORS)};
    var lightColors = ${JSON.stringify(LIGHT_PIECOLORS)};
    new Chart('${id}', {
        type : 'pie',
        data : {
            labels : data["${SUBNAME}"].map(function(row) { return row[fields["${SUBNAME}"][0]] }) ,
            datasets : fields["${SUBNAME}"].slice(1,2).map(function(field, index){
                return {
                    backgroundColor: colors,
                    hoverBackgroundColor: lightColors,
                    data: data["${SUBNAME}"].map(row => row[field]),
                }
            })
        }
    })`
    }
}