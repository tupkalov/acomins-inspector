const db = require(`${__rootdir}/db`)
const fs = require('fs')


const FOLDER = process.env.ACOMINS_IN_DOCKER ? '/deps/mount/subs/' : __rootdir + '/../mount/subs/';

function fetchSubsFiles () {

	return new Promise((resolve, reject) => {
		fs.readdir(FOLDER, (err, files) => {
			if (err) 
				return reject(err);

			resolve(files
				.filter(file => {
					return file.endsWith('.pug')
				})
				.map(file => {
					return file.split('.pug')[0]
				}));
		})
	})
}

function fetchQueries () {
	return db.getQueries()
}

const fetchSubs = co.wrap(function* () {
	const res = yield Promise.all([ fetchSubsFiles(), fetchQueries() ]);
	const files = res[0],
		  queries = res[1]

	const fetchObject = {}, fetchArray = [];

	Object.defineProperty(fetchArray, 'byName', {
		enumarable : false,
		value : fetchObject,
		writable : false
	})

	files.forEach(name => {
		fetchArray.push(
			fetchObject[name] = { name }
		)
	})

	queries.forEach(query => {
		const name = query.name,
			  querystring = query.querystring;

		if (fetchObject[name])
			fetchObject[name].querystring = querystring;

		else 
			fetchArray.push(
				fetchObject[name] = { name, querystring }
			)
	})


	return fetchArray;
})


const createSub = co.wrap(function* (query) {
	const name = query.name,
		querystring = query.querystring,
		template = query.template;

	const filePromise = changeTemplateByName(name, { template });

	const dbPromise = db.createQueryByName(name, { querystring })

	yield Promise.all([ filePromise, dbPromise ]);

	return { name, querystring, template };
})

function changeTemplateByName (name, query) {
	const template = query.template;
	
	return new Promise((resolve, reject) => 
			fs.open(`${FOLDER}${name}.pug`, 'w', (err, fd) => {
				if (err) reject(err)
				else resolve(fd)
			})
		)

		.then(fd => new Promise((resolve, reject) =>
			fs.writeFile(fd, template, (err) => {
				if (err) reject(err)
				else resolve()
			})
		))
}

function deleteTemplateByName (name) {
	return new Promise((resolve, reject) => 
		fs.unlink(`${FOLDER}${name}.pug`,  err => {
			err && console.error(err);
			resolve()
		})
	)
}

function findTemplateByName (name) {
	return new Promise((resolve, reject) => 
		fs.open(`${FOLDER}${name}.pug`, 'r', (err, fd) => {
			if (err) resolve('')
			else fs.readFile(fd, (err, data) => {
				if (err) reject(err);
				else resolve(data);
			})
		}))
}


module.exports = {
	fetchSubsFiles,
	fetchQueries,
	fetchSubs,
	createSub,
	changeTemplateByName,
	deleteTemplateByName,
	findTemplateByName
}