module.exports = {
	errors : {
		'CSV:NoDataForThisName' : 'Неправильное имя подшаблона для получения данных CSV'
	},

	signinError : message => {
		switch (message) {
			case 'signin:userNotFound':
			case 'signin:passwordNotCorrect':
				return "Логин или пароль неверны"
			default:
				return "Неизвестная ошибка"
		}
	},
	
	userCreateError : message => {
		switch (message) {
			case 'user/create:wrongLogin':
				return 'Введите логин пользователя'
			case 'user/create:wrongPassword':
				return "Валидные пароли не меньше 6 символов"
			case 'user/create:loginAlreadyExists':
				return "Пользователь с таким логином уже существует"
			case 'user/create:wrongName':
				return "Введите имя пользователя"
			default:
				return "Неизвестная ошибка"
		}
	},
	
	userChangeError : message => {
		switch (message) {
			case 'userChange:wrongLogin':
				return 'Введите логин пользователя'
			case 'userChange:wrongPassword':
				return "Валидные пароли не меньше 6 символов"
			case 'userChange:loginAlreadyExists':
				return "Пользователь с таким логином уже существует"
			case 'userChange:wrongName':
				return "Введите имя пользователя"
			case 'userChange:wrongTemplates':
				return "Неверный тип выбранных шаблонов" 
			default:
				return "Неизвестная ошибка"
		}
	},
	
	subCreateError : message => {
		switch (message) {
			case 'subs/create:wrongName':
				return 'Введите имя шаблона'
			case 'subs/create:nameAlreadyExists':
				return "Такое имя шаблона уже существует"
			case 'subs/create:noQuery':
				return "Введите текст запроса"
			case 'subs/create:wrongTemplate':
				return "Введите текст шаблона"
			default:
				return "Неизвестная ошибка"
		}
	},
	
	subChangeError : message => {
		switch (message) {
			case 'subChange:wrongQueryString':
				return "Неверная строка запроса";
			case 'subChange:wrongTemplate':
				return "Неверная строка шаблона"
			default:
				return "Неизвестная ошибка"
		}
	},
	
	templateCreateError : message => {
		switch (message) {
			case 'template/create:wrongName':
				return 'Введите имя страницы'
			case 'template/create:nameAlreadyExists':
				return "Такое имя страницы уже существует"
			case 'template/create:noContent':
				return "Вставьте шаблон"
			case 'template/create:noLabel':
				return "Вставьте выводимое имя страницы"
			default:
				return "Неизвестная ошибка"
		}
	},
	
	templateChangeError : message => {
		switch (message) {
			case 'templateChange:wrongTemplate':
				return "Неверная строка шаблона"
			case 'templateChange:wrongLabel':
				return "Неверное имя страницы";
			default:
				return "Неизвестная ошибка"
		}
	},


	errors : {
		'app/template:templateNotFound' : "Шаблон не найден",
		"app/template:noPermissions" : "Нет прав для просмотра этого шаблона",
		'app/template:wrongSubFormat' : "Один из инклюдов страницы имеет неправильный формат подключения подшаблона"
	},


	commonConfirmDelete : "Вы уверены что хотите удалить?"
}