import table from './table';
import chartcol from './chartcol';
import chartpie from './chartpie';
import chartline from './chartline';
import chartareal from './chartareal';

export default { table, chartcol, chartpie, chartline, chartareal };