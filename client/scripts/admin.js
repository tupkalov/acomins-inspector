import { confirm } from './modal'

export default () => {
	$('form[data-confirm]').each(function () {
		var confirmed;

		$(this).on('submit', event => {
			if (confirmed) return;

			event.preventDefault();
			confirm({ text : lang.commonConfirmDelete }).then((accept) => {
				if (!accept) return;
				
				confirmed = true;
				$(this).submit();
				setTimeout(() => {
					confirmed = false;
				}, 0)
			})
		})
	})
}