"use strict";

const authasher = require('authasher');
const db = require(`${__rootdir}/db`);

const fetchTemplatesFiles = require('../helpers/templateHelper').fetchTemplatesFiles;

module.exports = router => {

router.use('/users', generatorWrap(function* (req, res, next) {
	const users = yield db.getUsers();
	res.locals.users = users;
	res.locals.currentPage = 'users';
	next();
}))

router.get('/users', function (req, res, next) {

	res.render('allUsers');
})

router.use('/users/create', (req, res, next) => {
	res.locals.user = { id : 'create' };
	next()
})

router.get('/users/create', generatorWrap(function* (req, res, next) {
	res.render('createUser');
}))

router.post('/users/create', generatorWrap(function* (req, res, next) {
	var login = req.body.login,
		  name = req.body.name,
		  password = req.body.password;

	try {
		if (typeof login !== "string" || !(login = login.trim()).length)
			throw new AdminError('user/create:wrongLogin');

		const loginTry = yield db.getUserByLogin(login);
		if (loginTry)
			throw new AdminError('user/create:loginAlreadyExists')

		if (typeof password !== "string" || (password = password.trim()).length < 6)
			throw new AdminError('user/create:wrongPassword');

		if (typeof name !== "string" || !(name = name.trim()).length)
			throw new AdminError('user/create:wrongName');

		const passwordHash = authasher.create(password);

		const user = yield db.createUser({ login, passwordHash, name });


		res.redirect(`/admin/users/${user.id}`)
	} catch (error) {
		console.log(error);

		const localMessage = lang.userCreateError(error.message)
		res.locals.error = localMessage;
		res.locals.name = name;
		res.locals.login = login;

		res.render('createUser');
	}

}))

router.use('/users/:id', generatorWrap(function* (req, res, next) {
	const userPromise = db.getUserById(req.params.id);
	const templatesPromise = fetchTemplatesFiles()
	const retArr = yield Promise.all([ userPromise, templatesPromise ]);
	const user = retArr[0],
		  templates = retArr[1]

	if (!user) return res.redirect('/admin/users');

	res.locals.user = user;	
	res.locals.templates = templates;

	next()
}))


router.get('/users/:id', generatorWrap(function* (req, res, next) {
	res.render('user');
}))

router.post('/users/:id', generatorWrap(function* (req, res, next) {
	var name = req.body.name,
		  login = req.body.login,
		  password = req.body.password,
		  templates = req.body.templates,
		  deleteUser = req.body.deleteUser;

	if (login) {
		try {
			if (typeof login !== "string" || !(login = login.trim()).length)
				throw new AdminError('userChange:wrongLogin');

			if (login !== res.locals.user.login) {

				const loginTry = yield db.getUserByLogin(login);
				if (loginTry)
					throw new AdminError('userChange:loginAlreadyExists')

				let user = yield db.changeUserById(req.params.id, { login });
				res.locals.user = user;
			}
		} catch (error) {
			console.log(error);
			res.locals.loginError = lang.userChangeError(error.message);
		}


	} else if (name) {
		try {
			if (typeof name !== "string" || !(name = name.trim()).length)
				throw new AdminError('userChange:wrongName');

			if (name !== res.locals.user.name) {
				let user = yield db.changeUserById(req.params.id, { name });
				res.locals.user = user;
			}
		} catch (error) {
			console.log(error);
			res.locals.nameError = lang.userChangeError(error.message);
		}


	} else if (password) {

		try {
			if (typeof password !== "string" || (password = password.trim()).length < 6)
				throw new AdminError('userChange:wrongPassword');

			let passwordHash = authasher.create(password);

			let user = yield db.changeUserById(req.params.id, { passwordHash });
			res.locals.user = user;
		} catch (error) {
			console.log(error);
			res.locals.passwordError = lang.userChangeError(error.message);
		}
	} else if (templates) {

		if (typeof templates !== "object")
			throw new AdminError('userChange:wrongTemplates');

		const issetTemplates = (res.locals.templates || []).map(val => val.name);
		const templateNames = Object.keys(templates).filter(name => issetTemplates.indexOf(name) !== -1);

		const user = yield db.changeUserById(req.params.id, { templates : templateNames })
		res.locals.user = user;

	} else if (deleteUser) {
		yield db.deleteUserById(req.params.id);
		res.redirect('/admin/users')
	}

	res.render('user');
}))


}