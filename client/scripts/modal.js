const modalErrorMarkup = ({ text }) => {
	return `
		<div class="modal fade">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title">Авторизация</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <p>${text}</p>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>`
}

module.exports = ({ text }) => {
	$(modalErrorMarkup({ text })).modal({ show : true })
}


const modalConfirmMarkup = ({ text }) => `
		<div class="modal fade">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-body">
		        <p>${text}</p>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-role="ok">Да</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
		      </div>
		    </div>
		  </div>
		</div>`


module.exports.confirm = ({ text }) => {
	return new Promise((resolve, reject) => {
		$(modalConfirmMarkup({ text })).modal({ show : true })
			.on('click', '[data-role="ok"]', () => {
				resolve(true)
			})
			.on('hidden.bs.modal', () => {
				resolve()
			})
	});
}