"use strict";

const path = require('path');

global.__rootdir = __dirname;

global.co = require('co')

global.generatorWrap = callback => {
	return function (req,res,next) {
		co.wrap(callback)(req, res, next).catch(error => next(error))
	}
}

function LOG () {
	const trace = new Error().stack.split('\n')[2]
	console.log('\n\n\n'  + trace)
	console.dir.apply(console, arguments)
} 

global.lang = require(process.env.ACOMINS_IN_DOCKER ? '/common/lang' : '../common/lang')


Object.assign(global, 
{
	DEBUG : !!process.env.DEBUG,
	PORT : process.env.ACOMINS_FRONT_PORT || 80,
	SECRET  : "DFASDH352532523ASFYSFSAS",

	ADMIN_PASSWORD_HASH :  require('authasher').create(process.env.ACOMINS_ADMIN_PASSWORD || 'adminpassword'),
	ADMIN_LOGIN			: process.env.ACOMINS_ADMIN_LOGIN || 'admin',

	DB_JSON_PATH 		: path.resolve(__dirname, "../local-db/local-db.json"),
	SESSION_JSON_PATH 	: path.resolve(__dirname, ".."),
	SESSION_JSON_FILENAME : 'session-db.json',
	VIEWS_PATH 			: path.resolve(__dirname, "views")
})

const App = require('express');

const app = new App();
const middleware = require('./middleware')


app.use(App.static('../public'))

middleware(app);

app.listen(PORT);
console.log("listen " + PORT)