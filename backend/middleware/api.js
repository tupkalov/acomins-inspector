"use strict";

const Router = require('express').Router;

global.ApiError = HttpError.extend('Api');
const errors = [
	'signin:userNotFound',
	'signin:passwordNotCorrect'
]

const authasher = require('authasher');
const db = require(`${__rootdir}/db`);


const router = new Router();

router.post('/signin', generatorWrap(function* (req, res, next) {
	const login = req.body.login, 
		  password = req.body.password;

	var passwordHash, user;

	// find user
	if (login === ADMIN_LOGIN) 
	{
		passwordHash = ADMIN_PASSWORD_HASH;
	}
	else
	{
		user = yield db.getUserByLogin(login);
		user && (passwordHash = user.passwordHash);
		if (!passwordHash)
			throw new ApiError('signin:userNotFound')
	}

	const valid = authasher.validate(password, passwordHash).valid;
	if ( !valid )
		throw new ApiError('signin:passwordNotCorrect')

	Object.assign(req.session, {
		logged : true,
		login,
		user,
		rights : login === ADMIN_LOGIN ? 'admin' : 'member'
	});

	res.json({ type : 'ok', redirect : login === ADMIN_LOGIN ? '/admin' : '/app' })
}))


router.use(function (error, request, res, next) {

	if (error instanceof ApiError) {
		let status = error.status || 200;

		res.status(200).json({
			type : 'error',
			message : error.message
		})
	}else
		throw error;
	
})

module.exports = router;