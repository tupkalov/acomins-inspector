import uid from '../helpers/uid';
import gen from 'color-generator';


const COUNT_COLORS = 30;


        
export default {
    template () {
        // Colors
        const COLCOLORS = [];

        for (let left = COUNT_COLORS; left; left--) {
            let color = gen();
            COLCOLORS.push(color.hexString());
        }

        const id = uid();

        return `
canvas#${id}(width="400", height="200")
script.
    var colors = ${JSON.stringify(COLCOLORS)};
    new Chart('${id}', {
        type : 'line',
        data : {
            labels : data["${SUBNAME}"].map(function(row) { return row[fields["${SUBNAME}"][0]] }) ,
            datasets : fields["${SUBNAME}"].slice(1).map(function(field, index){
                return {
                    label: fields["${SUBNAME}"][0],
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: colors[index],
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: colors[index],
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: colors[index],
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: data["${SUBNAME}"].map(row => row[field]),
                    spanGaps: false,
                }
            })
        }
    })`
     }
}