const Router = require('express').Router;

global.AdminError = HttpError.extend('Admin');

const router = module.exports = new Router();

router.use(function(req, res, next) {
	if (!req.session.logged || req.session.rights !== "admin")
		res.redirect('/')
	else
		next();
})

router.get('/', (req, res, next) => {
	res.redirect('/admin/users');
})

require('./users')(router);
require('./subs')(router);
require('./templates')(router);