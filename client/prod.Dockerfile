FROM node:7.7.3

RUN npm install -g webpack@2.2.1

WORKDIR /
COPY package.json .
RUN npm i

WORKDIR /input

VOLUME /input
VOLUME /output
VOLUME /common

CMD webpack --config prod.webpack.config.js