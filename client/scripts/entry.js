import "chart.js";
import "whatwg-fetch";				// Полифил fetch
import initAuth from './auth';
import lang from '/common/lang';
import initAdmin from './admin';
import initEditor from './editor';
import initCSV from './csv';
import initApply from './apply';

global.lang = lang;

window.addEventListener('load', event => {
	initAuth();
	initAdmin()
	initEditor();
	initCSV();
	initApply();
})