import presets from './presets';

export default () => {


    [...document.querySelectorAll('[data-role="apply-preset"]')].forEach(el => {
        
        const { preset } = el.dataset;
        const presetHandler = presets[preset]
        if (!presetHandler) return

        const context = [...document.querySelectorAll('.form-group')].find(fGroup => fGroup.contains(el));
        const editorEl = context.querySelector('textarea + .code');
        const editor = EDITORS.get(editorEl);

        el.addEventListener('click', event => {
            event.preventDefault();
            editor.setValue(presetHandler.template())
        })
    })
}