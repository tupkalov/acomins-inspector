const templateHelper = require('../helpers/templateHelper')

const fetchTemplatesFiles = templateHelper.fetchTemplatesFiles,
	createTemplate = templateHelper.createTemplate,
	changeTemplateByName = templateHelper.changeTemplateByName,
	deleteTemplateByName = templateHelper.deleteTemplateByName,
	findTemplateByName = templateHelper.findTemplateByName;


module.exports = router => {





router.use('/templates', generatorWrap(function*(req, res, next) {

	const templates = yield fetchTemplatesFiles()
	res.locals.templates = templates;
	res.locals.currentPage = 'templates';
	next();
}))

router.get('/templates', generatorWrap(function*(req, res, next) {
	res.render('allTemplates');
}))



router.use('/templates/create', (req, res, next) => {
	res.locals.template = { id : 'create' };
	res.locals.template = '';
	next()
})

router.get('/templates/create', generatorWrap(function* (req, res, next) {
	res.render('createTemplate');
}))

router.post('/templates/create', generatorWrap(function* (req, res, next) {
	const name = req.body.name,
		string = req.body.string,
		label = req.body.label;

	try {
		if (typeof name !== "string" || !(name = name.trim()).length)
			throw new AdminError('template/create:wrongName');

		const nameTry = res.locals.templates.byName[name];
		if (nameTry)
			throw new AdminError('template/create:nameAlreadyExists')

		if (typeof string !== "string")
			throw new AdminError('template/create:noContent');

		if (typeof label !== "string" && !(label = label.trim()))
			throw new AdminError('template/create:noLabel');

		const template = yield createTemplate({ name, string, label });

		res.locals.template = template;


		res.redirect(`/admin/templates/${name}`)
	} catch (error) {
		console.log(error);

		const localMessage = lang.templateCreateError(error.message)
		res.locals.error = localMessage;
		res.locals.name = name;
		res.locals.string = string;

		res.render('createTemplate');
	}

}))

router.use('/templates/:name', generatorWrap(function* (req, res, next) {
	const template = res.locals.templates.byName[req.params.name];
	if (!template) return res.redirect('/admin/templates');

	Object.assign(template, yield findTemplateByName(template.name))
	res.locals.template = template;	
	next()
}))


router.get('/templates/:name', generatorWrap(function* users (req, res, next) {
	res.render('template');
}))

router.post('/templates/:name', generatorWrap(function* users (req, res, next) {
	const string = req.body.string,
		  label = req.body.label,
		  deleteTemplate = req.body.deleteTemplate;
		  
	if (string) {
		try {
			if (typeof string !== "string")
				throw new AdminError('templateChange:wrongTemplate');

			if (string !== res.locals.template.string) {
				yield changeTemplateByName(res.locals.template.name, { string });
				Object.assign(res.locals.template, { string });
			}
		} catch (error) {
			console.log(error);
			res.locals.stringeError = lang.templateChangeError(error.message);
		}

	} else if (label) {
		try {
			if (typeof label !== "string")
				throw new AdminError('templateChange:wrongLabel');

			if (label !== res.locals.template.label) {
				yield changeTemplateByName(res.locals.template.name, { label });
				Object.assign(res.locals.template, { label });
			}
		} catch (error) {
			console.log(error);
			res.locals.labelError = lang.templateChangeError(error.message);
		}

	} else if (deleteTemplate) {
		const name = res.locals.template.name;
		yield deleteTemplateByName(name);
		res.redirect('/admin/templates')
	}

	res.render('template');
}))

}